let path = require('path');
let express = require('express');
let app = express();

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {
    console.log(req.originalUrl);
    console.log(req.protocol);
    console.log(req.hostname);
    console.log(req.ip);
    console.log(req.path);
    console.log(req.query);
    let firstName = req.query.first;
    let lastName = req.query.last;
    let isStudent = req.query.isStudent;
    if (isStudent === undefined) isStudent = "OFF";
    res.send(firstName + ' ' + lastName + ' ' + isStudent);
});

app.listen(8080, function () {
    console.log('Example app listening on port 8080!');
});
